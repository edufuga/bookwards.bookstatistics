package com.gitlab.edufuga.bookwards.bookstatistics;

import com.gitlab.edufuga.wordlines.core.Status;
import com.gitlab.edufuga.wordlines.core.WordStatusDate;
import com.gitlab.edufuga.wordlines.recordreader.FileSystemRecordReader;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.ParseException;
import java.util.*;
import java.util.stream.Collectors;

public class BookStatistics {
    private final Path wordsFile;
    private final FileSystemRecordReader recordReader;

    private final Path splittedWordsFile;

    private final Path wordAndFrequencyFile;

    public BookStatistics(Path wordAndFrequencyFile, Path splittedWordsFile, Path wordsFile, String statusFolder) throws Exception {
        this.wordAndFrequencyFile = wordAndFrequencyFile;

        this.splittedWordsFile = splittedWordsFile;

        this.wordsFile = wordsFile;
        this.recordReader = new FileSystemRecordReader(statusFolder, null);
    }

    public void generate() throws IOException {
        List<String> words = Files.readAllLines(wordsFile);
        System.out.println("Number of words: " + words.size());

        List<WordStatusDate> records = words.stream().map(w -> {
            try {
                return recordReader.readRecord(w);
            } catch (IOException | ParseException e) {
                e.printStackTrace();
            }
            return null;
        })
                .filter(Objects::nonNull)
                .collect(Collectors.toList());

        List<WordStatusDate> presentRecords = records.stream()
                .filter(Objects::nonNull)
                .filter(w -> w.getStatus() != null)
                .collect(Collectors.toList());

        List<WordStatusDate> missingRecords = records.stream()
                .filter(Objects::nonNull)
                .filter(w -> w.getStatus() == null)
                .collect(Collectors.toList());

        int numberOfRecords = records.size();
        System.out.println("Number of records: " + numberOfRecords);
        System.out.println("");

        if (missingRecords.size() > 0) {
            String missingPercentage = String.format("%.2f", missingRecords.size() * 1.0 / numberOfRecords * 100.0);
            System.out.println("Number of missing records: " + missingRecords.size() + " (" + missingPercentage + "%)");
            System.out.println("");
        }

        if (presentRecords.size() > 0) {
            String presentPercentage = String.format("%.2f", presentRecords.size() * 1.0 / numberOfRecords * 100.0);
            System.out.println("Number of present records: " + presentRecords.size() + " (" + presentPercentage + "%)");
            System.out.println("");
        }

        System.out.println("Records by their status (percentages):");
        System.out.println();
        Map<Status, List<WordStatusDate>> recordsByStatus = presentRecords.stream()
                .collect(Collectors.groupingBy(WordStatusDate::getStatus));
        for (Map.Entry<Status, List<WordStatusDate>> entry : recordsByStatus.entrySet()) {
            Status status = entry.getKey();
            List<WordStatusDate> recordsForStatus = entry.getValue();
            String percentage = String.format("%.2f", recordsForStatus.size()*1.0/numberOfRecords * 100.0);
            String result = "" + status.name() + ": " + recordsForStatus.size() + " (" + percentage + "%)";
            System.out.println(result);
        }

        System.out.println("Records by their status (complete listings):");
        System.out.println();
        for (Map.Entry<Status, List<WordStatusDate>> entry : recordsByStatus.entrySet()) {
            Status status = entry.getKey();
            List<WordStatusDate> recordsForStatus = entry.getValue();
            String recordListForGivenStatus = recordsForStatus.stream().map(WordStatusDate::getWord).collect(Collectors.joining(", "));
            String result = "" + status.name() + ": " + recordListForGivenStatus;
            System.out.println(result);
        }

        System.out.println("");
        List<String> splittedWords = Files.readAllLines(splittedWordsFile);
        System.out.println("Number of splitted words: " + splittedWords.size());
        System.out.println("");

        evaluateDistribution(splittedWords);

        System.out.println("");
        evaluateDistributionOfStatesAccordingToWordFrequency(wordAndFrequencyFile);
    }

    private void evaluateDistribution(List<String> words) {
        // Distribution of each status AND of new aka missing words every X words (X=100, for example).
        // This will give an idea how many new words there are in a book and how they are distributed.
        // I guess that there will be a reasonable amount of new words in every (unprocessed) book no matter how many
        // words the vocabulary project has. I think there is always 'something' to learn in almost every book. I want
        // to *see* how it looks like instead of just thinking about it and guessing how it ought to be.
        int groupSize = 100;
        List<List<String>> wordsByGroupsOfX = new ArrayList<>();
        List<String> wordGroup = new ArrayList<>();
        for (String word : words)
        {
            if (wordGroup.size() < groupSize) {
                wordGroup.add(word);
            }
            else {
                wordsByGroupsOfX.add(wordGroup);
                wordGroup = new ArrayList<>();
            }
        }

        System.out.println("List of words by groups (unmodified book contents)");
        wordsByGroupsOfX.forEach(System.out::println);
        System.out.println("");

        // Now find out the distribution of new/old words.
        // This is like an ideal machine that knows nothing and learns everything it encounters.
        // Naively one would expect that the distribution falls off to zero at some point. Let's see if that's correct.
        List<List<String>> oldWordsDistribution = new ArrayList<>();
        List<List<String>> newWordsDistribution = new ArrayList<>();
        Set<String> oldWords = new HashSet<>();
        for (List<String> groupOfWords : wordsByGroupsOfX) {
            List<String> oldW = new ArrayList<>();
            List<String> newW = new ArrayList<>();

            for (String w : groupOfWords) {
                if (oldWords.contains(w)) {
                    oldW.add(w);
                }
                else {
                    newW.add(w);
                }

                oldWords.add(w);
            }

            oldWordsDistribution.add(oldW);
            newWordsDistribution.add(newW);
        }

        System.out.println("New words distribution:");
        newWordsDistribution.forEach(System.out::println);
        System.out.println("");

        System.out.println("Number of new words (per group size "+groupSize+").");
        List<Integer> numberOfNewWordsInEachGroup = newWordsDistribution.stream().map(List::size).collect(Collectors.toList());
        //System.out.println(numberOfNewWordsInEachGroup);

        // Tweak the number of new words in order to be able to visually recognise the pattern more easily.
        // This is a small displacement of the "real numbers" by a random amount.
        Random r = new Random();
        double rangeMin = -0.1;
        double rangeMax = +0.1;
        List<Double> tweakedNumberOfWordsInEachGroup = numberOfNewWordsInEachGroup.stream()
                .map(number -> {
                    double randomValue = rangeMin + (rangeMax - rangeMin) * r.nextDouble();
                    return number + randomValue;
                })
                .collect(Collectors.toList());
        tweakedNumberOfWordsInEachGroup.forEach(System.out::println);
    }

    private void evaluateDistributionOfStatesAccordingToWordFrequency(Path wordAndFrequencyFile) throws IOException {
        List<String> lines = Files.readAllLines(wordAndFrequencyFile);

        // Group words by frequency
        Map<Integer, List<String>> wordsByFrequency = new TreeMap<>();
        for (String wordAndFrequency : lines) {
            String[] wordAndFrequencySplitted = wordAndFrequency.split("\t");
            if (wordAndFrequencySplitted.length != 2) {
                continue;
            }

            String word = wordAndFrequencySplitted[0];
            String frequencyString = wordAndFrequencySplitted[1];
            Integer frequency = Integer.parseInt(frequencyString);

            wordsByFrequency.putIfAbsent(frequency, new ArrayList<>());
            wordsByFrequency.get(frequency).add(word);
        }

        // Count number of words with a given frequency
        Map<Integer, Integer> numberOfWordsByFrequency = new TreeMap<>();
        for (Map.Entry<Integer, List<String>> wordsByFrequencyEntry : wordsByFrequency.entrySet()) {
            Integer frequency = wordsByFrequencyEntry.getKey();
            List<String> wordsAtConcreteFrequency = wordsByFrequencyEntry.getValue();
            int numberOfWordsAtConcreteFrequency = wordsAtConcreteFrequency.size();
            numberOfWordsByFrequency.putIfAbsent(frequency, 0);

            // Update word count at frequency
            int oldNumberOfWords = numberOfWordsByFrequency.get(frequency);
            int newNumberOfWords = oldNumberOfWords + numberOfWordsAtConcreteFrequency;
            numberOfWordsByFrequency.put(frequency, newNumberOfWords);
        }

        // Output the "frequency:number_of_words" pairs
        System.out.println("Distribution [frequency:number_of_words]:");
        for (Map.Entry<Integer, Integer> frequencyAndNumberOfWordsEntry : numberOfWordsByFrequency.entrySet()) {
            Integer frequency = frequencyAndNumberOfWordsEntry.getKey();
            Integer numberOfWords = frequencyAndNumberOfWordsEntry.getValue();

            System.out.println(frequency+"\t"+numberOfWords);
        }

        // Now do the same for every record *state*. I want the number of words in a certain state, per frequency.
        Map<Integer, List<WordStatusDate>> recordsByFrequency = new TreeMap<>();

        for (Map.Entry<Integer, List<String>> wordsByFrequencyEntry : wordsByFrequency.entrySet()) {
            Integer frequency = wordsByFrequencyEntry.getKey();
            List<String> wordsAtConcreteFrequency = wordsByFrequencyEntry.getValue();
            List<WordStatusDate> recordsAtConcreteFrequency = wordsAtConcreteFrequency.stream().map(w -> {
                try {
                    return recordReader.readRecord(w);
                } catch (IOException | ParseException e) {
                    e.printStackTrace();
                }
                throw new RuntimeException("Shouldn't happen!");
            })
                    .filter(Objects::nonNull)
                    .filter(w -> w.getStatus() != null)
                    .collect(Collectors.toList());

            recordsByFrequency.put(frequency, recordsAtConcreteFrequency);
        }

        Map<Status, Map<Integer, Integer>> numberOfWordsWithAGivenFrequencyAndState = new TreeMap<>();
        for (Map.Entry<Integer, List<WordStatusDate>> recordsByFrequencyEntry : recordsByFrequency.entrySet()) {
            Integer frequency = recordsByFrequencyEntry.getKey();
            List<WordStatusDate> recordsWithFrequency = recordsByFrequencyEntry.getValue();

            for (WordStatusDate recordWithFrequency : recordsWithFrequency) {
                Status state = recordWithFrequency.getStatus();
                numberOfWordsWithAGivenFrequencyAndState.putIfAbsent(state, new TreeMap<>());
                Map<Integer, Integer> distributionForState = numberOfWordsWithAGivenFrequencyAndState.get(state);
                distributionForState.putIfAbsent(frequency, 0);
                int oldValue = distributionForState.get(frequency);
                int newValue = oldValue + 1; // This is simply a single record with the given state.
                distributionForState.put(frequency, newValue);
            }
        }

        for (Status state : Status.values()) {
            System.out.println("");
            System.out.println("Distribution [frequency:count] for " + state.name());
            if (!numberOfWordsWithAGivenFrequencyAndState.containsKey(state)) {
                continue;
            }
            Map<Integer, Integer> distributionForState = numberOfWordsWithAGivenFrequencyAndState.get(state);
            for (Map.Entry<Integer, Integer> frequencyAndNumberOfWordsEntry : distributionForState.entrySet()) {
                Integer frequency = frequencyAndNumberOfWordsEntry.getKey();
                Integer numberOfWords = frequencyAndNumberOfWordsEntry.getValue();

                System.out.println(frequency+"\t"+numberOfWords);
            }
        }

        // FIXME: The "new" are missing. These are the "fake" records with a "null" state.
        System.out.println("");
        // Now make a table of frequency, total count, and individual count for each state.
        Map<Integer, Map<Status, Integer>> tableOfFrequencyToIndividualStateCounts = new TreeMap<>();
        System.out.print("Frequency\t");
        System.out.print("Total\t");
        for (Status state : numberOfWordsWithAGivenFrequencyAndState.keySet()) {
            System.out.print(state.name()+"\t");
        }
        System.out.println("");

        for (Integer frequency : numberOfWordsByFrequency.keySet()) {
            System.out.print(frequency+"\t");
            Integer totalCount = numberOfWordsByFrequency.get(frequency);
            if (totalCount == null) {
                totalCount = 0;
            }
            System.out.print(totalCount+"\t");
            for (Status state : numberOfWordsWithAGivenFrequencyAndState.keySet()) {
                Map<Integer, Integer> countsByFrequency = numberOfWordsWithAGivenFrequencyAndState.get(state);
                Integer individualCountForState = countsByFrequency.get(frequency);

                if (individualCountForState == null) {
                    individualCountForState = 0;
                }
                System.out.print(individualCountForState+"\t");
            }
            System.out.println("");
        }

        /* Cumulated distribution */
        System.out.println("Cumulated Distribution");
        System.out.println("");

        // FIXME: The "new" are missing. These are the "fake" records with a "null" state.
        int cumulatedTotalCount = 0;
        Map<Status, Integer> cumulatedStateCounts = new HashMap<>();
        System.out.println("");
        // Now make a table of frequency, total count, and individual count for each state.
        System.out.print("Frequency\t");
        System.out.print("Cumulated_Total\t");
        for (Status state : numberOfWordsWithAGivenFrequencyAndState.keySet()) {
            System.out.print(state.name()+"\t");
        }
        System.out.println("");

        for (Integer frequency : numberOfWordsByFrequency.keySet()) {
            System.out.print(frequency+"\t");
            cumulatedTotalCount += frequency * (numberOfWordsByFrequency.get(frequency) == null ? 0 : numberOfWordsByFrequency.get(frequency));
            System.out.print(cumulatedTotalCount+"\t");
            for (Status state : numberOfWordsWithAGivenFrequencyAndState.keySet()) {
                Map<Integer, Integer> countsByFrequency = numberOfWordsWithAGivenFrequencyAndState.get(state);

                cumulatedStateCounts.putIfAbsent(state, 0);

                Integer oldStateCount = cumulatedStateCounts.get(state) == null ? 0 : cumulatedStateCounts.get(state);
                Integer delta = countsByFrequency.get(frequency) == null ? 0 : countsByFrequency.get(frequency);
                Integer newStateCount = oldStateCount + delta;
                cumulatedStateCounts.put(state, newStateCount);

                System.out.print(newStateCount+"\t");
            }
            System.out.println("");
        }
    }

    public static void main(String[] args) throws Exception {
        if (args.length != 4) {
            throw new RuntimeException("Expected parameters: word and frequency file path, splitted file path, words file path, status folder");
        }

        Path wordAndFrequencyFile = Paths.get(args[0]);
        Path wordsWithRepetitions = Paths.get(args[1]);
        Path words = Paths.get(args[2]);
        String statusFolder = args[3];

        BookStatistics bookStatistics = new BookStatistics(wordAndFrequencyFile, wordsWithRepetitions, words, statusFolder);
        bookStatistics.generate();
    }
}
